# Olivia Stream

Streaming Spotify Playlists through NFC on a Raspberry Pi

### What's required

- Raspberry Pi
- NFC Shield
- Spotify Premium (To stream playlists in order)

# Installation

To run the process
