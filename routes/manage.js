var express = require('express');
var router = express.Router();
const playlists = require('../db/playlists.json');

console.log(playlists);

router.get('/', function (req, res, next) {
  res.render('manage', { title: 'Manage', playlists });
});

module.exports = router;
